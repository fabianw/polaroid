// File       : OrganizerMPI.h
// Date       : Thu Apr 28 09:53:58 2016
// Author     : Fabian Wermelinger
// Description: MPI Organizer
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef ORGANIZERMPI_H_8HFBYG90
#define ORGANIZERMPI_H_8HFBYG90

#include <vector>
#include <string>
#include <mpi.h>

#include "ArgumentParser.h"

#ifdef _FLOAT_PRECISION_
#define MPIReal MPI_FLOAT
#else
#define MPIReal MPI_DOUBLE
#endif

class OrganizerMPI
{
private:
    int m_nscenes;
    std::vector<std::string> m_scenes;
    int m_argc;
    const MPI_Comm m_comm;
    bool m_isroot;

public:
    OrganizerMPI(const int argc, char ** const argv, const MPI_Comm comm=MPI_COMM_WORLD);

    inline int size() const
    {
        int size;
        MPI_Comm_size(m_comm, &size);
        return size;
    }

    inline int rank() const
    {
        int rank;
        MPI_Comm_rank(m_comm, &rank);
        return rank;
    }

    std::vector<std::string> split_work() const;
    inline int argc() const { return m_argc; }
    inline bool isroot() const { return m_isroot; }
    inline void wait() const { MPI_Barrier(m_comm); }
    inline void allreduce(void* send, void* recv, int count, MPI_Datatype type, MPI_Op op)
    {
        MPI_Allreduce(send, recv, count, type, op, m_comm);
    }
};

#endif /* ORGANIZERMPI_H_8HFBYG90 */
