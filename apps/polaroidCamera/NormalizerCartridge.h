// File       : NormalizerCartridge.h
// Date       : Thu Apr 28 09:32:20 2016
// Author     : Fabian Wermelinger
// Description: Data Normalization Cartridge
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef NORMALIZERCARTRIDGE_H_ONAUDSVX
#define NORMALIZERCARTRIDGE_H_ONAUDSVX

#include <algorithm>
#include "Cartridge.h"

class NormalizerCartridge : public Cartridge
{
public:
    NormalizerCartridge(ArgumentParser& parser) : Cartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        photo.make_new(photo.get_name()+"-normalizer", data.width(), data.height());

        // set description
        string desc("2D_Normalized");
        photo.set_description(desc.c_str());

        // compute min/max for shader
        if (!m_bComputed)
        {
            m_dataMin = data.min();
            m_dataMax = data.max();
        }
        const Real data_normInv = 1.0 / (m_dataMax - m_dataMin);

        // pixel shader
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
                photo.set_pixel((data(w,h) - m_dataMin) * data_normInv, w, h);

        photo.write();
    }

    virtual void compute(Slice& data)
    {
        m_dataMin = std::min(m_dataMin, data.min());
        m_dataMax = std::max(m_dataMax, data.max());
        m_bComputed = true;
    }
};

#endif /* NORMALIZERCARTRIDGE_H_ONAUDSVX */
