// File       : PhotoASCII.cpp
// Date       : Thu 30 Jun 2016 04:14:56 PM CEST
// Author     : Fabian Wermelinger
// Description: 1D ASCII paper Implementation
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <cassert>
#include <fstream>
#include "PhotoASCII.h"

using namespace std;

void PhotoASCII::make_new(const string name, const int N, const int dummy)
{
    m_fname = name;
    m_data.clear();
    m_data.resize(N);
}

void PhotoASCII::resize(const int N, const int dummy)
{
    m_data.clear();
    m_data.resize(N);
}

void PhotoASCII::write()
{
    ofstream asciiout(m_fname.c_str());
    asciiout.setf(std::ios::scientific, std::ios::floatfield);
    asciiout.precision(12);
    if (!m_data.empty())
    {
        const Real h = 1.0/m_data.size();
        for (size_t i = 0; i < m_data.size(); ++i)
            asciiout << h*(i+0.5) << '\t' << m_data[i] << endl;
    }
    asciiout.close();
}

void PhotoASCII::set_pixel(const double phi, const int i, const int dummy)
{
    assert(i < static_cast<int>(data.size()));
    m_data[i] = phi;
}
