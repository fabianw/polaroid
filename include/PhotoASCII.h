// File       : PhotoASCII.h
// Date       : Thu 30 Jun 2016 03:58:09 PM CEST
// Author     : Fabian Wermelinger
// Description: ASCII .dat file for 1D extraction
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef PHOTOASCII_H_OSYU1497
#define PHOTOASCII_H_OSYU1497

#include <string>
#include <vector>

#include "common.h"
#include "Polaroid.h"
#include "PhotoPaper.h"

class PhotoASCII : public PhotoPaper
{
private:
    std::vector<Real> m_data;
    Real m_time;

public:
    PhotoASCII(const int N=0, const std::string filename="dat1d", const Real t=0) : PhotoPaper(0,0,filename), m_data(N), m_time(t)
    {
        m_description = "ASCII data";
    }
    virtual ~PhotoASCII() { }

    virtual void make_new(const std::string name, const int N, const int dummy);
    virtual void resize(const int N, const int dummy);
    virtual void write();
    virtual void set_pixel(const double phi, const int x, const int dummy);
    virtual std::string suffix() const { return std::string(".dat"); }
    inline void set_time(const Real t) { m_time = t; }
};

#endif /* PHOTOASCII_H_OSYU1497 */
