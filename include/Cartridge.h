// File       : Cartridge.h
// Date       : Wed Apr 27 21:17:10 2016
// Author     : Fabian Wermelinger
// Description: Simple Transmission Cartridge Module for Polaroid Camera
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef CARTRIDGE_H_IRUVKOCT
#define CARTRIDGE_H_IRUVKOCT

#include <cmath>
#include "ArgumentParser.h"
#include "Types.h"
#include "PhotoPaper.h"

class Cartridge
{
protected:
    ArgumentParser& m_parser;
    bool m_bComputed;

    Real m_dataMin;
    Real m_dataMax;

public:
    Cartridge(ArgumentParser& parser) : m_parser(parser), m_bComputed(false), m_dataMin(HUGE_VAL), m_dataMax(-HUGE_VAL) {}
    virtual ~Cartridge() {}

    virtual void capture(PhotoPaper& photo, Slice& data) = 0;
    virtual void compute(Slice& data) = 0;
    virtual void reset()
    {
        m_bComputed = false;
        m_dataMin = HUGE_VAL;
        m_dataMax = -HUGE_VAL;
    }

    inline Real min() const { return m_dataMin; }
    inline Real max() const { return m_dataMax; }
    inline void set_min(const Real v) { m_dataMin = v; }
    inline void set_max(const Real v) { m_dataMax = v; }
};

#endif /* CARTRIDGE_H_IRUVKOCT */
