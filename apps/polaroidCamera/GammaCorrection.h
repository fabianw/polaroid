// File       : GammaCorrection.h
// Date       : Sun 01 May 2016 11:22:38 PM CEST
// Author     : Fabian Wermelinger
// Description: Gamma Correction Filter
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef GAMMACORRECTION_H_4SYE23BR
#define GAMMACORRECTION_H_4SYE23BR

#include <cmath>
#include "ArgumentParser.h"

class GammaCorrection
{
private:
    ArgumentParser& m_parser;

public:
    GammaCorrection(ArgumentParser& parser) : m_parser(parser) {}

    inline Real operator()(const Real v) const
    {
        const Real gamma = m_parser("-gamma").asDouble(1.0);
        const Real A     = m_parser("-gammaA").asDouble(1.0);
        return A*std::pow(v, gamma);
    }
};

#endif /* GAMMACORRECTION_H_4SYE23BR */
