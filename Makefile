include ./Makefile.config

CC = mpic++

HDR = $(wildcard src/*.h)
SRC = $(wildcard src/*.cpp)
OBJ = ${SRC:.cpp=.o}

INC += -Iapps/polaroidCamera
INC += -Iapps

APPSRC = $(wildcard apps/polaroidCamera/*.cpp)
APPSRC += $(wildcard apps/*.cpp)
APPOBJ = ${APPSRC:.cpp=.o}

.PHONY: clean cleanThird cleanAll

polaroidCamera: third_party Polaroid $(APPOBJ)
	$(CC) $(CPPFLAGS) $(INC) -o bin/polaroidCamera $(APPOBJ) -lPolaroid $(LIB) -lpng -lz

Polaroid: third_party $(HDR) $(OBJ)
	ar rcs lib/libPolaroid.a $(OBJ)
	ranlib lib/libPolaroid.a

third_party: FORCE
	$(MAKE) -C third_party all

FORCE:

%.o: %.cpp
	$(CC) $(CPPFLAGS) $(INC) -c $< -o $@

clean:
	find . -iname "*~" -exec rm -f {} \;
	rm -f $(OBJ)
	rm -f $(APPOBJ)
	rm -f lib/libPolaroid.a
	rm -f bin/polaroidCamera

cleanThird:
	$(MAKE) -C third_party clean

cleanAll: clean cleanThird
