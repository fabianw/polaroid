// File       : Types.h
// Date       : Tue Apr 26 14:49:41 2016
// Author     : Fabian Wermelinger
// Description: Various types
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef TYPES_H_QATFIWCK
#define TYPES_H_QATFIWCK

#include <cassert>
#include <algorithm>
#include <cmath>
#include "common.h"

template <int _SX, int _EX, int _SY, int _EY>
class Slice2D
{
    int m_width, m_height, m_N;
    Real* m_data;

    int m_max3Ddim;
    int m_sliceID;
    char m_sliceDir;

public:
    Slice2D() : m_width(0), m_height(0), m_N(0), m_data(nullptr) {}
    Slice2D(const int width, const int height) : m_width(width), m_height(height), m_N((width+_EX-_SX)*(height+_EY-_SY))
    {
        m_data = new Real[m_N];
        // for (int i = 0; i < m_N; ++i)
        //     m_data[i] = 0.0;
    }
    virtual ~Slice2D() { delete [] m_data; }

    inline Real operator()(const int ix, const int iy) const
    {
        assert(ix >= _SX); assert(ix < m_width+_EX);
        assert(iy >= _SY); assert(iy < m_height+_EY);

        return m_data[(iy-_SY)*m_width + (ix-_SX)];
    }

    inline Real& operator()(const int ix, const int iy)
    {
        assert(ix >= _SX); assert(ix < m_width+_EX);
        assert(iy >= _SY); assert(iy < m_height+_EY);

        return m_data[(iy-_SY)*m_width + (ix-_SX)];
    }

    inline void resize(const int width, const int height)
    {
        m_width = width;
        m_height = height;
        m_N = (width+_EX-_SX)*(height+_EY-_SY);
        if (m_data) delete [] m_data;
        m_data = new Real[m_N];
        // for (int i = 0; i < m_N; ++i)
        //     m_data[i] = 0.0;
    }

    inline int width() const { return m_width; }
    inline int height() const { return m_height; }
    inline Real min() const
    {
        Real f = m_data[0];
        for (int i = 1; i < m_N; ++i)
            f = std::min(f, m_data[i]);
        return f;
    }
    inline Real max() const
    {
        Real f = m_data[0];
        for (int i = 1; i < m_N; ++i)
            f = std::max(f, m_data[i]);
        return f;
    }
    inline void set_max3Ddim(const int d) { m_max3Ddim = d; }
    inline int get_max3Ddim() const { return m_max3Ddim; }
    inline void set_sliceID(const int d) { m_sliceID = d; }
    inline int get_sliceID() const { return m_sliceID; }
    inline void set_sliceDir(const char d) { m_sliceDir = d; }
    inline char get_sliceDir() const { return m_sliceDir; }
};

typedef Slice2D<0,0,0,0> Slice;


class Slice2D_Statistics
{
    double m_mean; // 1st moment
    double m_var;  // 2nd moment
    double m_skew; // 3rd moment
    double m_kurt; // 4th moment
    double m_min, m_max;

    void _compute_stat(const Slice& s)
    {
        m_min = s.min();
        m_max = s.max();

        int k = 0;
        double mean = 0;
        double M2 = 0, M3 = 0, M4 = 0;
        for (int h=0; h < s.height(); ++h)
            for (int w=0; w < s.width(); ++w)
            {
                const int k1 = k;
                ++k;
                const double delta = s(w,h) - mean;
                const double delta_k = delta / k;
                const double delta_k2 = delta_k * delta_k;
                const double term1 = delta * delta_k * k1;
                mean += delta_k;
                M4 += term1 * delta_k2 * (k*k - 3*k + 3) + 6 * delta_k2 * M2 - 4 * delta_k * M3;
                M3 += term1 * delta_k * (k - 2) - 3 * delta_k * M2;
                M2 += term1;
            }
        assert(k > 1);

        m_mean = mean;
        m_var  = M2 / (k - 1);
        m_skew = std::sqrt(k) * M3 / std::pow(M2, 1.5);
        m_kurt = k * M4 / (M2 * M2) - 3;
    }

public:
    Slice2D_Statistics(const Slice& s) :
        m_mean(0), m_var(0), m_skew(0), m_kurt(0), m_min(HUGE_VAL), m_max(-HUGE_VAL)
    {
        _compute_stat(s);
    }

    // accessors
    double mean() const { return m_mean; }
    double var()  const { return m_var; }
    double std()  const { return std::sqrt(m_var); }
    double skew() const { return m_skew; }
    double kurt() const { return m_kurt; }
    double min()  const { return m_min; }
    double max()  const { return m_max; }
};

#endif /* TYPES_H_QATFIWCK */
