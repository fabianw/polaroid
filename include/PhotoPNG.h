// File       : PhotoPNG.h
// Date       : Tue Apr 26 14:46:18 2016
// Author     : Fabian Wermelinger
// Description: PNG Photos
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef PHOTOPNG_H_7DZT9TLW
#define PHOTOPNG_H_7DZT9TLW

#include "Polaroid.h"
#include "PhotoPaper.h"
#include "pngwriter.h"

class PNG_HSV : public PhotoPaper
{
protected:
    bool m_open;
    pngwriter* m_png;
    double m_saturation, m_value, m_background;
    std::string m_title, m_author, m_software;

    // helper
    inline void _default_info()
    {
        m_title = "Portable Network Graphics";
        m_author = "Walt Disney";
        m_software = "Polaroid++";
    }
    inline void _dispose() { delete m_png; m_png=nullptr; }

public:

    PNG_HSV(const std::string filename="hsv", const double saturation=1.0, const double value=1.0, const double bg=1.0) :
        PhotoPaper(0,0,filename), m_open(false), m_png(nullptr), m_saturation(saturation), m_value(value), m_background(bg)
    {
        m_description = "PNG HSV colorscheme";
        _default_info();
    }
    PNG_HSV(const Polaroid& cam, const std::string filename="hsv", const double saturation=1.0, const double value=1.0, const double bg=1.0) :
        PhotoPaper(0,0,filename), m_open(false), m_png(nullptr), m_saturation(saturation), m_value(value), m_background(bg)
    {
        m_description = "PNG HSV colorscheme";
        _default_info();
        resize(cam.width(), cam.height());
    }
    PNG_HSV(const int width, const int height, const std::string filename="hsv", const double saturation=1.0, const double value=1.0, const double bg=1.0) :
        PhotoPaper(0,0,filename), m_open(false), m_png(nullptr), m_saturation(saturation), m_value(value), m_background(bg)
    {
        m_description = "PNG HSV colorscheme";
        _default_info();
        resize(width, height);
    }

    virtual ~PNG_HSV()
    {
        if (m_open)
        {
            m_png->close();
            _dispose();
        }
    }

    virtual void make_new(const std::string name, const int width, const int height);
    virtual void resize(const int width, const int height);
    virtual void write();
    virtual void set_pixel(const double phi, const int x, const int y);
    virtual std::string suffix() const { return std::string(".png"); }
};

class PNG_MONO : public PNG_HSV
{
public:
    PNG_MONO(const std::string filename="mono") : PNG_HSV(filename) { m_description = "PNG MONO colorscheme"; }
    PNG_MONO(const Polaroid& cam, const std::string filename="mono") : PNG_HSV(cam, filename) { m_description = "PNG MONO colorscheme"; }
    PNG_MONO(const int width, const int height, const std::string filename="mono") : PNG_HSV(width, height, filename) { m_description = "PNG MONO colorscheme"; }

    virtual void set_pixel(const double phi, const int x, const int y);
};

class PNG_RGB : public PNG_HSV
{
    PNG_RGB(const std::string filename="rgb") : PNG_HSV(filename) { m_description = "PNG RGB colorscheme"; }
    PNG_RGB(const Polaroid& cam, const std::string filename="rgb") : PNG_HSV(cam, filename) { m_description = "PNG RGB colorscheme"; }
    PNG_RGB(const int width, const int height, const std::string filename="rgb") : PNG_HSV(width, height, filename) { m_description = "PNG RGB colorscheme"; }

    inline void set_pixel(const int x, const int y, const double R, const double G, const double B)
    {
        if (m_open)
            m_png->plot(x+1, y+1, R, G, B);
    }
};

#endif /* PHOTOPNG_H_7DZT9TLW */
