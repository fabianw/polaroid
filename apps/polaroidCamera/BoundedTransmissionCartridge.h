// File       : BoundedTransmissionCartridge.h
// Date       : Fri 29 Apr 2016 09:22:10 AM CEST
// Author     : Fabian Wermelinger
// Description: Data BoundedTransmission Cartridge
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef BOUNDEDTRANSMISSIONCARTRIDGE_H_YDXLKPJU
#define BOUNDEDTRANSMISSIONCARTRIDGE_H_YDXLKPJU

#include <algorithm>
#include "TransmissionCartridge.h"

class BoundedTransmissionCartridge : public TransmissionCartridge
{
public:
    BoundedTransmissionCartridge(ArgumentParser& parser) : TransmissionCartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        const Real upper = m_parser("-upper_bound").asDouble(1.0);
        const Real lower = m_parser("-lower_bound").asDouble(0.0);

        photo.make_new(photo.get_name()+"-boundedTransmission", data.width(), data.height());

        // set description
        string desc("2D_Bounded_Transmission");
        photo.set_description(desc.c_str());

        // pixel shader
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
            {
                const Real bound = std::max(lower,std::min(upper,data(w,h)));
                photo.set_pixel(bound, w, h);
            }

        photo.write();
    }
};

#endif /* BOUNDEDTRANSMISSIONCARTRIDGE_H_YDXLKPJU */
