// File       : Cartridges.h
// Date       : Wed Apr 27 21:37:25 2016
// Author     : Fabian Wermelinger
// Description: Cartridge Collection
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef CARTRIDGES_H_LTWKNANR
#define CARTRIDGES_H_LTWKNANR

#include "TransmissionCartridge.h"
#include "NormalizerCartridge.h"
#include "LogNormalizerCartridge.h"
#include "BoundedTransmissionCartridge.h"
#include "BoundedNormalizerCartridge.h"
#include "BoundedLogNormalizerCartridge.h"
#include "SchlierenCartridge.h"
#include "LineExtractor.h"
#include "SliceStat.h"

#endif /* CARTRIDGES_H_LTWKNANR */
