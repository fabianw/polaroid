// File       : BoundedLogNormalizerCartridge.h
// Date       : Fri 29 Apr 2016 09:39:39 AM CEST
// Author     : Fabian Wermelinger
// Description: Bounded Log Data Normalizer Cartridge
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef BOUNDEDLOGNORMALIZERCARTRIDGE_H_QF9H4ZPF
#define BOUNDEDLOGNORMALIZERCARTRIDGE_H_QF9H4ZPF

#include <cassert>
#include <algorithm>
#include <cmath>
#include "BoundedNormalizerCartridge.h"

class BoundedLogNormalizerCartridge : public BoundedNormalizerCartridge
{
public:
    BoundedLogNormalizerCartridge(ArgumentParser& parser) : BoundedNormalizerCartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        const Real upper = m_parser("-upper_bound").asDouble(10.0);
        const Real lower = m_parser("-lower_bound").asDouble(1.0);

        photo.make_new(photo.get_name()+"-boundedLogNormalizer", data.width(), data.height());

        // set description
        string desc("2D_Bounded_Log_Normalized");
        photo.set_description(desc.c_str());

        // compute min/max for shader
        const Real dataMinInv = 1.0/lower;
        const Real fac = 1.0/log(upper*dataMinInv);
        assert(!isnan(dataMinInv));
        assert(!isnan(fac));

        // pixel shader
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
            {
                const Real bound = std::max(static_cast<Real>(0.0), std::min(static_cast<Real>(1.0), log(data(w,h)*dataMinInv)*fac));
                assert(!isnan(bound));
                photo.set_pixel(bound, w, h);
            }

        photo.write();
    }
};

#endif /* BOUNDEDLOGNORMALIZERCARTRIDGE_H_QF9H4ZPF */
