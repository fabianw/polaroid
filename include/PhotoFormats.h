// File       : PhotoFormats.h
// Date       : Wed Apr 27 21:39:13 2016
// Author     : Fabian Wermelinger
// Description: Photo Format Collection
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef PHOTOFORMATS_H_3IWQHPAU
#define PHOTOFORMATS_H_3IWQHPAU

#include "PhotoPaper.h"
#include "PhotoHDF5.h"
#include "PhotoPNG.h"
#include "PhotoASCII.h"
#include "PhotoINFO.h"

#endif /* PHOTOFORMATS_H_3IWQHPAU */
