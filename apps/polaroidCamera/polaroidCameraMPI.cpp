// File       : polaroidCameraMPI.cpp
// Date       : Wed Apr 27 14:40:05 2016
// Author     : Fabian Wermelinger
// Description: Polaroid Cam app
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <iostream>
#include <cstdio>
#include <vector>
#include <string>

#include "ArgumentParser.h"
#include "OrganizerMPI.h"
#include "SceneProcessor.h"


using namespace std;

int main(int argc, char* argv[])
{
    MPI_Init(&argc, (char***)&argv);

    OrganizerMPI worker(argc, argv);
    ArgumentParser myparser(worker.argc(), (const char**)argv);

    if (worker.isroot())
    {
        cout << "Command Line: ";
        for (int i = 0; i < argc; ++i)
            cout << argv[i] << " ";
        cout << endl;
        myparser.print_args();
        cout.flush();
    }
    worker.wait();

    vector<string> myscenes = worker.split_work();
    if (myscenes.size() > 0)
        printf("[Worker %d/%d: Load = %d scene(s), start @ %s]\n", worker.rank(), worker.size(), myscenes.size(), myscenes.front().c_str());
    else
        printf("[Worker %d/%d: Load = %d scene(s), start @ %s]\n", worker.rank(), worker.size(), myscenes.size(), "none");

    SceneProcessor myprocessor(myparser, worker);

    if (myparser("-process").asInt(1212) == 1212)
        myprocessor.process1212(myscenes);
    else if (myparser("-process").asInt(1212) == 1122)
        myprocessor.process1122(myscenes);
    else
    {
        if (worker.isroot())
            cerr << "ERROR: Undefined processing \"" << myparser("-process").asInt(1212) << "\"" << endl;
        abort();
    }

    worker.wait();
    MPI_Finalize();

    return 0;
}
