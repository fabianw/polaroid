// File       : TransmissionCartridge.h
// Date       : Thu Apr 28 09:29:22 2016
// Author     : Fabian Wermelinger
// Description: Simple Data Transmission Cartridge
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef TRANSMISSIONCARTRIDGE_H_Z4VDKHDO
#define TRANSMISSIONCARTRIDGE_H_Z4VDKHDO

#include "Cartridge.h"

class TransmissionCartridge : public Cartridge
{
public:
    TransmissionCartridge(ArgumentParser& parser) : Cartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        photo.make_new(photo.get_name()+"-transmission", data.width(), data.height());

        // set description
        string desc("2D_Transmission");
        photo.set_description(desc.c_str());

        // put pixel
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
                photo.set_pixel(data(w,h), w, h);

        photo.write();
    }

    virtual void compute(Slice& data) {}
};

#endif /* TRANSMISSIONCARTRIDGE_H_Z4VDKHDO */
