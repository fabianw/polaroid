// File       : PhotoINFO.cpp
// Date       : Tue 12 Jul 2016 04:33:18 PM CEST
// Author     : Fabian Wermelinger
// Description: PhotoINFO implementation
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <cassert>
#include <fstream>
#include <iomanip>
#include "PhotoINFO.h"

using namespace std;

void PhotoINFO::make_new(const string name, const int dummy1, const int dummy2)
{
    m_fname = name;
    m_buf.str().clear();
}

void PhotoINFO::write(const Slice2D_Statistics& stat)
{
    ofstream info(m_fname.c_str());
    info.setf(std::ios::scientific, std::ios::floatfield);
    info.precision(12);
    info << setfill('.') << setw(32) << left << "Mean" << ": " << stat.mean() << endl;
    info << setfill('.') << setw(32) << left << "Variance" << ": " << stat.var() << endl;
    info << setfill('.') << setw(32) << left << "Standard deviation" << ": " << stat.std() << endl;
    info << setfill('.') << setw(32) << left << "Skewness" << ": " << stat.skew() << endl;
    info << setfill('.') << setw(32) << left << "Kurtosis" << ": " << stat.kurt() << endl;
    info << setfill('.') << setw(32) << left << "Minimum" << ": " << stat.min() << endl;
    info << setfill('.') << setw(32) << left << "Maximum" << ": " << stat.max() << endl;
    info.close();
}
