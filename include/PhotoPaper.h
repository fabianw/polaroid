// File       : PhotoPaper.h
// Date       : Tue Apr 26 14:45:19 2016
// Author     : Fabian Wermelinger
// Description: Photo Paper Base
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef PHOTOPAPER_H_B0K8P9TO
#define PHOTOPAPER_H_B0K8P9TO

#include <string>
#include "Types.h"

class PhotoPaper
{
protected:
    int m_width, m_height;
    std::string m_fname;
    std::string m_description;

public:
    PhotoPaper(const int width, const int height, const std::string& name) : m_width(width), m_height(height), m_fname(name), m_description("data") {};
    virtual ~PhotoPaper() {};

    virtual void make_new(const std::string name, const int width=1, const int height=0) = 0;
    virtual void resize(const int width, const int height=0) = 0;
    virtual void write() = 0;
    virtual void set_pixel(const double phi, const int x, const int y=0) = 0;
    virtual std::string suffix() const = 0;
    virtual void set_description(const char* const desc) { m_description = std::string(desc); }
    inline void set_name(const std::string& name) { m_fname = name; }
    inline std::string get_name() const { return m_fname; }
};

#endif /* PHOTOPAPER_H_B0K8P9TO */
