// File       : OrganizerMPI.cpp
// Date       : Thu Apr 28 10:59:03 2016
// Author     : Fabian Wermelinger
// Description: MPI Organizer Implementation
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <string>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include "OrganizerMPI.h"
#include "ArgumentParser.h"

using namespace std;

OrganizerMPI::OrganizerMPI(const int argc, char ** const argv, const MPI_Comm comm) : m_nscenes(0), m_argc(1), m_comm(comm)
{
    m_isroot = (rank() == 0) ? true : false;
    ArgumentParser p(argc, (const char**)argv);

    // get scenes
    bool bfoundScenes = false;
    for (int i=1; i < argc; ++i)
    {
        const string key(argv[i]);
        if (key == "-scenes")
        {
            char ** const scenes = (argv + (i+1));
            m_nscenes = argc - (i+1);
            bfoundScenes = true;
            for (int i = 0; i < m_nscenes; ++i)
                m_scenes.push_back(string(scenes[i]));
            break;
        }
        ++m_argc;
    }
    if (p.exist("fscenes"))
    {
        ifstream infile(p("fscenes").asString());
        string line;
        int count = 0;
        while (infile >> line)
        {
            m_scenes.push_back(line);
            ++count;
        }
        m_nscenes += count;
        bfoundScenes = true;
    }

    if (!bfoundScenes)
    {
        // Define input scenes (=input files) at the end of the argument list
        // by using the option "-scenes". You can use globbing here if there
        // are many scenes.
        if (m_isroot)
        {
            cerr << "ERROR: No input scenes given. Abort..." << endl;
            cerr << "     : (Note: -scenes must be the last argument, followed by [list of] input files)" << endl;
        }
        abort();
    }
}

vector<string> OrganizerMPI::split_work() const
{
    const int myrank = rank();
    const int nranks = size();
    const int myshare = m_nscenes/nranks;
    const int rem = m_nscenes - nranks * myshare;
    vector<string> myscenes(0);
    if (m_nscenes <= nranks)
    {
        if (myrank < m_nscenes) myscenes.push_back(m_scenes[myrank]);
        return myscenes;
    }

    for (int i=0; i < myshare; ++i) // try to distribute work equally
        myscenes.push_back(m_scenes[i*nranks + myrank]);
    if (myrank < rem)
        myscenes.push_back(m_scenes[myshare*nranks + myrank]);
    return myscenes;
}
