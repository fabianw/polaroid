// File       : common.h
// Date       : Tue Apr 26 22:13:00 2016
// Author     : Fabian Wermelinger
// Description: Common stuff
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef COMMON_H_13EQ6YAI
#define COMMON_H_13EQ6YAI

#ifdef _FLOAT_PRECISION_
typedef float Real;
#else
typedef double Real;
#endif

#ifdef _USE_HDF_
#ifdef _FLOAT_PRECISION_
#define HDF_PRECISION H5T_NATIVE_FLOAT
#else
#define HDF_PRECISION H5T_NATIVE_DOUBLE
#endif
#endif /* _USE_HDF_ */

#endif /* COMMON_H_13EQ6YAI */
