// File       : SchlierenCartridge.h
// Date       : Thu 28 Apr 2016 04:45:20 PM CEST
// Author     : Fabian Wermelinger
// Description: Schlieren Image Cartridge
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef SCHLIERENCARTRIDGE_H_AUHX7ILM
#define SCHLIERENCARTRIDGE_H_AUHX7ILM

#include <cstdio>
#include <algorithm>
#include <cmath>
#include <functional>
#include "Cartridge.h"
#include "GammaCorrection.h"

class SchlierenCartridge : public Cartridge
{
    Real m_mean;
    Real m_std;
    size_t m_k;
    Slice m_gradX;
    Slice m_gradY;

    // helper
    void _compute(Slice& data);
    void _gradX(const Slice& data);
    void _gradY(const Slice& data);

public:
    SchlierenCartridge(ArgumentParser& parser) : Cartridge(parser), m_mean(0.0), m_std(0.0), m_k(0) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        const Real ka0 = m_parser("-ka0").asDouble(1.0);
        const Real ka1 = m_parser("-ka1").asDouble(1.0);
        // const Real k0 = m_parser("-k0").asDouble(0.0);
        // const Real k1 = m_parser("-k1").asDouble(1.0);
        GammaCorrection gammaCorrect(m_parser);

        photo.make_new(photo.get_name()+"-schlieren", data.width(), data.height());

        // set description
        string desc("2D_Schlieren");
        photo.set_description(desc.c_str());

        // compute schlieren
        if (!m_bComputed)
        {
            _compute(data);
            m_dataMin = data.min();
            m_dataMax = data.max();
        }
        const Real dataMaxInv = 1.0/m_dataMax;
        const Real dataMaxInv_log = 1.0/std::log(m_dataMax+1.0);

        // const Real fac = -ka0/(k1 - k0);
        // auto exp_shader     = [&](const Real x) { return std::exp(fac*(x*dataMaxInv - k0)); };

        auto exp_shader     = [&](const Real x) { return std::exp(-ka0*x*dataMaxInv); };

        auto exp_shader_log = [&](const Real x)
        {
            const Real psi = std::log(x+1.0) * dataMaxInv_log;
            return std::exp(-ka1*psi);
        };

        auto blend_shaders = [&](const Real x)
        {
            const Real beta = (x-m_dataMin)/(m_dataMax-m_dataMin);
            return beta*exp_shader(x) + (1.0-beta)*exp_shader_log(x);
        };

        std::function<Real(const Real)> _shader = exp_shader;
        if (m_parser("-blend").asBool(false))
            _shader = blend_shaders;

        // apply shader
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
            {
                const Real phi = _shader(data(w,h));
                photo.set_pixel(gammaCorrect(phi), w, h);
            }

        photo.write();
    }

    virtual void compute(Slice& data)
    {
        _compute(data);
        m_dataMin = std::min(m_dataMin, data.min());
        m_dataMax = std::max(m_dataMax, data.max());
        m_bComputed = true;
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
            {
                ++m_k;
                const Real delta = data(w,h) - m_mean;
                m_mean += delta/m_k;
                m_std  += delta*(data(w,h) - m_mean);
            }
    }

    virtual void reset()
    {
        Cartridge::reset();
        m_mean = 0.0;
        m_std  = 0.0;
    }
};

#endif /* SCHLIERENCARTRIDGE_H_AUHX7ILM */
