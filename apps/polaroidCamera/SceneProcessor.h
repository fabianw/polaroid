// File       : SceneProcessor.h
// Date       : Thu 28 Apr 2016 05:08:33 PM CEST
// Author     : Fabian Wermelinger
// Description: Scene Processing
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef SCENEPROCESSOR_H_UWGYBVIW
#define SCENEPROCESSOR_H_UWGYBVIW

#include <vector>
#include <string>
#include <sstream>
#include <set>

#include "ArgumentParser.h"
#include "OrganizerMPI.h"
#include "Polaroid.h"
#include "Cartridges.h"
#include "PhotoFormats.h"

class SceneProcessor
{
private:
    ArgumentParser& m_parser;
    OrganizerMPI& m_mpi;
    Cartridge* m_cartridge;
    PhotoPaper* m_photo;

    // helper
    void _prepare_cam();
    void _load_cam(Polaroid& cam, const char* const fname) const;
    inline void _dispose()
    {
        if (m_cartridge) delete m_cartridge;
        if (m_photo) delete m_photo;
        m_cartridge = nullptr;
        m_photo = nullptr;
    }

    std::vector<std::string> _splitpath(const std::string& str, const char delim='/')
    {
        std::vector<std::string> result;
        const std::set<char> delimiters{delim};
        std::string path("");

        char const* pch = str.c_str();
        char const* start = pch;
        int shift = 0;
        for(; *pch; ++pch)
        {
            if (delimiters.find(*pch) != delimiters.end())
            {
                if (start != pch)
                {
                    std::string subpath(start, pch);
                    path += subpath;
                    start = pch;
                    shift = 1;
                }
            }
        }
        result.push_back(path);
        result.push_back(start+shift);
        return result;
    }

    std::string _outpath(const std::string& input, const char delim='/')
    {
        const std::vector<std::string> splitp = _splitpath(input, delim);
        std::ostringstream output;
        if (m_parser.exist("outpath"))
            output << m_parser("outpath").asString() << delim << splitp[1];
        else
            output << splitp[1];
        return output.str();
    }

public:
    SceneProcessor(ArgumentParser& parser, OrganizerMPI& mpi) : m_parser(parser), m_mpi(mpi), m_cartridge(nullptr), m_photo(nullptr) {}
    ~SceneProcessor() { _dispose(); }

    void process1212(const std::vector<std::string>& scenes);
    void process1122(const std::vector<std::string>& scenes);
};

#endif /* SCENEPROCESSOR_H_UWGYBVIW */
