// File       : BoundedNormalizerCartridge.h
// Date       : Fri 29 Apr 2016 09:33:09 AM CEST
// Author     : Fabian Wermelinger
// Description: Bounded Normalizer Cartridge
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef BOUNDEDNORMALIZERCARTRIDGE_H_DYS5RKGA
#define BOUNDEDNORMALIZERCARTRIDGE_H_DYS5RKGA

#include <algorithm>
#include "Cartridge.h"

class BoundedNormalizerCartridge : public Cartridge
{
public:
    BoundedNormalizerCartridge(ArgumentParser& parser) : Cartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        const Real upper = m_parser("-upper_bound").asDouble(1.0);
        const Real lower = m_parser("-lower_bound").asDouble(0.0);

        photo.make_new(photo.get_name()+"-boundedNormalizer", data.width(), data.height());

        // set description
        string desc("2D_Bounded_Normalized");
        photo.set_description(desc.c_str());

        const Real data_normInv = 1.0 / (upper - lower);

        // pixel shader
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
            {
                const Real bound = std::max(static_cast<Real>(0.0),std::min(static_cast<Real>(1.0), (data(w,h)-lower)*data_normInv));
                photo.set_pixel(bound, w, h);
            }

        photo.write();
    }

    virtual void compute(Slice& data) { }
};

#endif /* BOUNDEDNORMALIZERCARTRIDGE_H_DYS5RKGA */
