// File       : LineExtractor.h
// Date       : Thu 30 Jun 2016 02:12:10 PM CEST
// Author     : Fabian Wermelinger
// Description: Straight Line extraction tool
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef LINEEXTRACTOR_H_R2VFDR8H
#define LINEEXTRACTOR_H_R2VFDR8H

#include <string>
#include <sstream>
#include <cassert>
#include <algorithm>
#include <iostream>
#include "Cartridge.h"

class LineExtractor: public Cartridge
{
public:
    LineExtractor(ArgumentParser& parser) : Cartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        assert(photo.suffix() == std::string(".dat"));

        const int width = data.width();
        const int height = data.height();

        const Real wf = m_parser("-wf").asDouble(-1.0);
        const int wi  = m_parser("-wi").asInt(-1);
        const Real hf = m_parser("-hf").asDouble(-1.0);
        const int hi  = m_parser("-hi").asInt(-1);

        // set description
        string desc("1D_Line");
        photo.set_description(desc.c_str());

        const std::string basename(photo.get_name());

        if ((wf >= 0 && wi < 0) || (wi >= 0 && wf < 0))
        {
            const int fixed = (wf >= 0) ? static_cast<int>(width*wf) : wi;
            assert(fixed < width);
            std::ostringstream buf;
            buf << "-line_sliceDir=" << data.get_sliceDir() << "_sliceID=" << data.get_sliceID();
            buf << "_sliceWidthID=" << fixed;
            photo.make_new(basename+buf.str()+photo.suffix(), height);

            // extract line
            for (int h=0; h < height; ++h)
                photo.set_pixel(data(fixed,h), h);
            photo.write();
        }
        if ((hf >= 0 && hi < 0) || (hi >= 0 && hf < 0))
        {
            const int fixed = (hf >= 0) ? static_cast<int>(height*hf) : hi;
            assert(fixed < height);
            std::ostringstream buf;
            buf << "-line_sliceDir=" << data.get_sliceDir() << "_sliceID=" << data.get_sliceID();
            buf << "_sliceHeightID=" << fixed;
            photo.make_new(basename+buf.str()+photo.suffix(), width);

            // extract line
            for (int w=0; w < width; ++w)
                photo.set_pixel(data(w,fixed), w);
            photo.write();
        }
        if ((wf < 0 && wi < 0) && (hi < 0 && hf < 0))
            std::cerr << "No seed point specified for line extraction... Skipping this one" << std:: endl;
    }

    virtual void compute(Slice& data) {}
};

#endif /* LINEEXTRACTOR_H_R2VFDR8H */
