// File       : SceneProcessor.cpp
// Date       : Thu 28 Apr 2016 05:43:50 PM CEST
// Author     : Fabian Wermelinger
// Description: Scene Processor implementation
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <string>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <chrono>
#include "SceneProcessor.h"

using namespace std;

void SceneProcessor::_prepare_cam()
{
    const string cart = m_parser("-cartridge").asString("transmission");
    if (cart == "transmission")
        m_cartridge = new TransmissionCartridge(m_parser);
    else if (cart == "normalizer")
        m_cartridge = new NormalizerCartridge(m_parser);
    else if (cart == "log_normalizer")
        m_cartridge = new LogNormalizerCartridge(m_parser);
    else if (cart == "bounded_transmission")
        m_cartridge = new BoundedTransmissionCartridge(m_parser);
    else if (cart == "bounded_normalizer")
        m_cartridge = new BoundedNormalizerCartridge(m_parser);
    else if (cart == "bounded_log_normalizer")
        m_cartridge = new BoundedLogNormalizerCartridge(m_parser);
    else if (cart == "schlieren")
        m_cartridge = new SchlierenCartridge(m_parser);
    else if (cart == "line_extractor")
        m_cartridge = new LineExtractor(m_parser);
    else if (cart == "statistics")
        m_cartridge = new SliceStat(m_parser);
    else
    {
        if (m_mpi.isroot())
            cerr << "ERROR: Unknown cartridge \"" << cart << "\"" << endl;
        abort();
    }

    const string paper = m_parser("-photo").asString("png_hsv");
    if (paper == "png_hsv")
        m_photo = new PNG_HSV;
    else if (paper == "png_mono")
        m_photo = new PNG_MONO;
    else if (paper == "h5")
        m_photo = new PhotoHDF5;
    else if (paper == "ascii")
        m_photo = new PhotoASCII;
    else if (paper == "info")
        m_photo = new PhotoINFO;
    else
    {
        if (m_mpi.isroot())
            cerr << "ERROR: Unknown photo paper\"" << paper << "\"" << endl;
        abort();
    }
}

void SceneProcessor::_load_cam(Polaroid& cam, const char* const fname) const
{
    const string input_type = m_parser("-input").asString("h5");

    if (input_type == "h5")
        cam.load_hdf5(fname, m_parser);
    else if (input_type == "h5_slice")
        cam.load_hdf5_slice(fname, m_parser);
    else if (input_type == "wavelet")
        cam.load_wavelet(fname, m_parser);
    else
    {
        if (m_mpi.isroot())
            cerr << "ERROR: Undefined input type \"" << input_type << "\"" << endl;
        abort();
    }
}

void SceneProcessor::process1212(const vector<string>& scenes)
{
    _prepare_cam();
    Polaroid mycam(m_cartridge);

    const auto start = chrono::high_resolution_clock::now();
    for (size_t i=0; i<scenes.size(); ++i)
    {
        _load_cam(mycam, scenes[i].c_str());

        std::string basename = _outpath(scenes[i]);
        m_photo->set_name(basename);
        mycam.capture(*m_photo);

        if (m_mpi.isroot())
        {
            const auto now = chrono::high_resolution_clock::now();
            const auto delta = now - start;
            const double sec = chrono::duration_cast<chrono::seconds>(delta).count();
            printf("[Progress %3.1f %% (elapsed %.1f s)]\n", static_cast<double>(i+1)/scenes.size()*100.0, sec);
        }
    }
}

void SceneProcessor::process1122(const vector<string>& scenes)
{
    _prepare_cam();
    vector<Polaroid> mycams(scenes.size());

    // 1.) load scenes & compute
    const auto start = chrono::high_resolution_clock::now();
    for (size_t i=0; i<scenes.size(); ++i)
    {
        Polaroid& cam = mycams[i];
        _load_cam(cam, scenes[i].c_str());
        cam.insertCartridge(m_cartridge);
        cam.computeScene();
        if (m_mpi.isroot())
        {
            const auto now = chrono::high_resolution_clock::now();
            const auto delta = now - start;
            const double sec = chrono::duration_cast<chrono::seconds>(delta).count();
            printf("[Scene Progress %3.1f %% (elapsed %.1f s)]\n", static_cast<double>(i+1)/scenes.size()*100.0, sec);
        }
    }

    Real globalMin, globalMax;
    Real myMin = m_cartridge->min();
    Real myMax = m_cartridge->max();
    m_mpi.allreduce(&myMin, &globalMin, 1, MPIReal, MPI_MIN);
    m_mpi.allreduce(&myMax, &globalMax, 1, MPIReal, MPI_MAX);
    m_cartridge->set_min(globalMin);
    m_cartridge->set_max(globalMax);

    // 2.)apply pixel shader and write photo
    for (size_t i=0; i<scenes.size(); ++i)
    {
        Polaroid& cam = mycams[i];

        std::string basename = _outpath(scenes[i]);
        m_photo->set_name(basename);
        cam.capture(*m_photo);

        if (m_mpi.isroot())
        {
            const auto now = chrono::high_resolution_clock::now();
            const auto delta = now - start;
            const double sec = chrono::duration_cast<chrono::seconds>(delta).count();
            printf("[Shader Progress %3.1f %% (elapsed %.1f s)]\n", static_cast<double>(i+1)/scenes.size()*100.0, sec);
        }
    }
}
