// File       : LogNormalizerCartridge.h
// Date       : Fri 29 Apr 2016 09:05:30 AM CEST
// Author     : Fabian Wermelinger
// Description: Log Normalizer Cartridge
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef LOGNORMALIZERCARTRIDGE_H_GEKZAQWI
#define LOGNORMALIZERCARTRIDGE_H_GEKZAQWI

#include <cassert>
#include <cmath>
#include "NormalizerCartridge.h"

class LogNormalizerCartridge : public NormalizerCartridge
{
public:
    LogNormalizerCartridge(ArgumentParser& parser) : NormalizerCartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        photo.make_new(photo.get_name()+"-logNormalizer", data.width(), data.height());

        // set description
        string desc("2D_Log_Normalized");
        photo.set_description(desc.c_str());

        // compute min/max for shader
        if (!m_bComputed)
        {
            m_dataMin = data.min();
            m_dataMax = data.max();
        }
        const Real dataMinInv = 1.0/m_dataMin;
        const Real fac = 1.0/log(m_dataMax*dataMinInv);
        assert(!isnan(dataMinInv));
        assert(!isnan(fac));

        // pixel shader
        for (int h=0; h < data.height(); ++h)
            for (int w=0; w < data.width(); ++w)
            {
                const Real logData = log(data(w,h)*dataMinInv);
                assert(!isnan(logData));
                photo.set_pixel(fac*logData, w, h);
            }

        photo.write();
    }
};

#endif /* LOGNORMALIZERCARTRIDGE_H_GEKZAQWI */
