// File       : SliceStat.h
// Date       : Tue 12 Jul 2016 03:05:34 PM CEST
// Author     : Fabian Wermelinger
// Description: Extract statistics of current slice
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef SLICEINFO_H_FGI1MKEH
#define SLICEINFO_H_FGI1MKEH

#include <string>
#include <sstream>
#include <cassert>
#include "Cartridge.h"
#include "Types.h"
#include "PhotoINFO.h"

class SliceStat: public Cartridge
{
public:
    SliceStat(ArgumentParser& parser) : Cartridge(parser) {}

    virtual void capture(PhotoPaper& photo, Slice& data)
    {
        assert(photo.suffix() == std::string(".info"));
        PhotoINFO& pi = dynamic_cast<PhotoINFO&>(photo);

        // set description
        string desc("2D_Slice_Info");
        pi.set_description(desc.c_str());

        const std::string basename(pi.get_name());

        std::ostringstream buf;
        buf << "-info_sliceDir=" << data.get_sliceDir() << "_sliceID=" << data.get_sliceID();
        pi.make_new(basename+buf.str()+pi.suffix());

        // collect statistics
        Slice2D_Statistics stat(data);
        pi.write(stat);
    }

    virtual void compute(Slice& data) {}
};

#endif /* SLICEINFO_H_FGI1MKEH */
