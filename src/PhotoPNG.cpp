// File       : PhotoPNG.cpp
// Date       : Wed Apr 27 22:46:32 2016
// Author     : Fabian Wermelinger
// Description: PNG Photos implementation
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include "PhotoPNG.h"

void PNG_HSV::make_new(const string name, const int width, const int height)
{
    m_fname = name;
    resize(width, height);
}

void PNG_HSV::resize(const int width, const int height)
{
    if (m_open)
    {
        m_png->close();
        _dispose();
    }
    m_png = new pngwriter(width, height, m_background, (m_fname+this->suffix()).c_str());

    m_width = width;
    m_height = height;
    m_open = true;
}

void PNG_HSV::write()
{
    if (m_open)
    {
        m_png->settext(m_title.c_str(), m_author.c_str(), m_description.c_str(), m_software.c_str());
        m_png->write_png();
        m_png->close();
        _dispose();
        m_open = false;
    }
}

void PNG_HSV::set_pixel(const double phi, const int x, const int y)
{
    if (m_open)
    {
        const double hue = 2./3. * (1.0 - phi);
        m_png->plotHSV(x+1, y+1, hue, m_saturation, m_value);
    }
}

void PNG_MONO::set_pixel(const double phi, const int x, const int y)
{
    if (m_open)
        m_png->plot(x+1, y+1, phi, phi, phi);
}
