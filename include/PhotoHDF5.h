// File       : PhotoHDF5.h
// Date       : Tue Apr 26 23:24:00 2016
// Author     : Fabian Wermelinger
// Description: HDF5 Photo Paper
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef PHOTOHDF5_H_VN6WHSEO
#define PHOTOHDF5_H_VN6WHSEO

#ifdef _USE_HDF_
#include <hdf5.h>
#include <string>

#include "common.h"
#include "Polaroid.h"
#include "PhotoPaper.h"

class PhotoHDF5 : public PhotoPaper
{
private:
    bool m_open;
    Real* m_hdfraw;
    Real m_time;

    // HDF 5 types
    hid_t m_hdf5_fileid;
    hsize_t m_hdf5_count[4];
    hsize_t m_hdf5_dims[4];
    hsize_t m_hdf5_offset[4];

    // helper
    void _open_hdf_file();
    void _close_hdf_file();
    void _dump_xmf() const;

public:
    PhotoHDF5(const std::string filename="hdf5", const Real t=0) : PhotoPaper(0,0,filename), m_open(false), m_hdfraw(nullptr), m_time(t) {}
    PhotoHDF5(const Polaroid& cam, const std::string filename="hdf5", const Real t=0) : PhotoPaper(0,0,filename), m_open(false), m_hdfraw(nullptr), m_time(t)
    {
        resize(cam.width(), cam.height());
    }
    PhotoHDF5(const int width, const int height, const std::string filename="hdf5", const Real t=0) : PhotoPaper(0,0,filename), m_open(false), m_hdfraw(nullptr), m_time(t)
    {
        resize(width,height);
    }
    virtual ~PhotoHDF5() { if (m_hdfraw) delete [] m_hdfraw; }

    virtual void make_new(const std::string name, const int width, const int height);
    virtual void resize(const int width, const int height);
    virtual void write();
    virtual void set_pixel(const double phi, const int x, const int y);
    virtual std::string suffix() const { return std::string(".h5"); }
    inline void set_time(const Real t) { m_time = t; }
};
#endif /* _USE_HDF_ */

#endif /* PHOTOHDF5_H_VN6WHSEO */
