// File       : Polaroid.cpp
// Date       : Tue Apr 26 22:14:08 2016
// Author     : Fabian Wermelinger
// Description: Polaroid Implementation
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <cassert>
#include <cmath>
#include <cstring>
#include <algorithm>
#include <string>
#include <sstream>
#include <cstdlib>
#ifdef _USE_HDF_
#include <hdf5.h>
#endif /* _USE_HDF_ */

#include "Polaroid.h"
#ifdef _USE_CUBISMZ_
#include "Reader_WaveletCompression.h"
#endif /* _USE_CUBISMZ_ */

using namespace std;

#define ID3(ix, iy, iz, NX, NY) ((ix) + (NX) * ((iy) + (NY) * (iz)))

template <typename T>
inline void _streamSwap(const size_t n, unsigned char * const stream)
{
    constexpr size_t _e = sizeof(T);
    unsigned char _buf[_e];
    for (size_t i = 0; i < n*_e; i += _e)
    {
        const unsigned char * const upstream = stream + i + (_e - 1);
#pragma unroll(_e)
        for (int j = 0; j < _e; ++j)
            _buf[j] = *(upstream - j);
        std::memcpy(stream+i, &_buf[0], _e);
    }
}

void Polaroid::load_hdf5(const char* filename, ArgumentParser& parser)
{
#ifdef _USE_HDF_
    const double sf      = parser("-sf").asDouble(0.5); // slice fraction (default)
    const int si         = parser("-si").asInt(-1); // slice index (need to specify)
    const char direction = parser("-sd").asString("x")[0]; // slice normal direction
    const int channel    = parser("-channel").asInt(0); // data channel
    const bool magnitude = parser("-magnitude").asBool(false); // vector magnitude (only if NCH == 3)
    const bool swap      = parser("-swap").asBool(false); // swap bytes?

    /* open data */
    hid_t file_id, dataset_id, dataspace_id, file_dataspace_id;
    hsize_t* dims;
    hssize_t num_elem;
    int rank, ndims, NCH;
    int maxDim[3];
    Real* data;

    file_id           = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
    dataset_id        = H5Dopen(file_id, "/data", H5P_DEFAULT);
    file_dataspace_id = H5Dget_space(dataset_id);
    rank              = H5Sget_simple_extent_ndims(file_dataspace_id);
    dims              = new hsize_t[rank];
    ndims             = H5Sget_simple_extent_dims(file_dataspace_id, dims, NULL);
    num_elem          = H5Sget_simple_extent_npoints(file_dataspace_id);
    data              = new Real[num_elem];
    maxDim[2]         = dims[0];
    maxDim[1]         = dims[1];
    maxDim[0]         = dims[2];
    NCH               = dims[3];
    dataspace_id      = H5Screate_simple(rank, dims, NULL);
    int status        = H5Dread(dataset_id, HDF_PRECISION, dataspace_id, file_dataspace_id, H5P_DEFAULT, data);

    const int Nmax = std::max(maxDim[0], std::max(maxDim[1], maxDim[2]));
    m_data.set_max3Ddim(Nmax);

    /* release stuff */
    delete [] dims;
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    status = H5Sclose(file_dataspace_id);
    status = H5Fclose(file_id);

    assert(channel < NCH);

    /* extract plane */
    m_data.set_sliceDir(direction);
    if ('x' == direction) // y-z plane
    {
        const int fixed = (si >= 0) ? si : static_cast<int>(maxDim[0]*sf);
        m_data.set_sliceID(fixed);
        m_data.resize(maxDim[1], maxDim[2]);
        if (magnitude && NCH == 3)
        {
            for (int h=0; h < m_data.height(); ++h)
                for (int w=0; w < m_data.width(); ++w)
                {
                    const Real a = data[0 + NCH * ID3(fixed,w,h,maxDim[0], maxDim[1])];
                    const Real b = data[1 + NCH * ID3(fixed,w,h,maxDim[0], maxDim[1])];
                    const Real c = data[2 + NCH * ID3(fixed,w,h,maxDim[0], maxDim[1])];
                    m_data(w,h) = std::sqrt(a*a + b*b + c*c);
                }
        }
        else
        {
            for (int h=0; h < m_data.height(); ++h)
                for (int w=0; w < m_data.width(); ++w)
                    m_data(w,h) = data[channel + NCH * ID3(fixed,w,h,maxDim[0], maxDim[1])];
        }
    }
    else if ('y' == direction) // x-z plane
    {
        const int fixed = (si >= 0) ? si : static_cast<int>(maxDim[1]*sf);
        m_data.set_sliceID(fixed);
        m_data.resize(maxDim[0], maxDim[2]);
        if (magnitude && NCH == 3)
        {
            for (int h=0; h < m_data.height(); ++h)
                for (int w=0; w < m_data.width(); ++w)
                {
                    const Real a = data[0 + NCH * ID3(w,fixed,h,maxDim[0], maxDim[1])];
                    const Real b = data[1 + NCH * ID3(w,fixed,h,maxDim[0], maxDim[1])];
                    const Real c = data[2 + NCH * ID3(w,fixed,h,maxDim[0], maxDim[1])];
                    m_data(w,h) = std::sqrt(a*a + b*b + c*c);
                }
        }
        else
        {
            for (int h=0; h < m_data.height(); ++h)
                for (int w=0; w < m_data.width(); ++w)
                    m_data(w,h) = data[channel + NCH * ID3(w,fixed,h,maxDim[0], maxDim[1])];
        }
    }
    else if ('z' == direction) // x-y plane
    {
        const int fixed = (si >= 0) ? si : static_cast<int>(maxDim[2]*sf);
        m_data.set_sliceID(fixed);
        m_data.resize(maxDim[0], maxDim[1]);
        if (magnitude && NCH == 3)
        {
            for (int h=0; h < m_data.height(); ++h)
                for (int w=0; w < m_data.width(); ++w)
                {
                    const Real a = data[0 + NCH * ID3(w,h,fixed,maxDim[0], maxDim[1])];
                    const Real b = data[1 + NCH * ID3(w,h,fixed,maxDim[0], maxDim[1])];
                    const Real c = data[2 + NCH * ID3(w,h,fixed,maxDim[0], maxDim[1])];
                    m_data(w,h) = std::sqrt(a*a + b*b + c*c);
                }
        }
        else
        {
            for (int h=0; h < m_data.height(); ++h)
                for (int w=0; w < m_data.width(); ++w)
                    m_data(w,h) = data[channel + NCH * ID3(w,h,fixed,maxDim[0], maxDim[1])];
        }
    }
    else
    {
        fprintf(stderr, "ERROR: Direction '%c' unsupported.", direction);
        abort();
    }

    delete [] data;
    m_dataLoaded = true;

#else
    fprintf(stderr, "WARNING: Executable was compiled without HDF support...\n");
#endif /* _USE_HDF_ */
}

void Polaroid::load_hdf5_slice(const char* filename, ArgumentParser& parser)
{
#ifdef _USE_HDF_
    const int channel    = parser("-channel").asInt(0); // data channel
    const bool magnitude = parser("-magnitude").asBool(false); // vector magnitude (only if NCH == 3)
    const bool swap      = parser("-swap").asBool(false); // swap bytes?

    /* open data */
    hid_t file_id, dataset_id, dataspace_id, file_dataspace_id;
    hsize_t* dims;
    hssize_t num_elem;
    int rank, ndims, NCH;
    int maxDim[2];
    Real* data;

    file_id           = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
    dataset_id        = H5Dopen(file_id, "/data", H5P_DEFAULT);
    file_dataspace_id = H5Dget_space(dataset_id);
    rank              = H5Sget_simple_extent_ndims(file_dataspace_id);
    dims              = new hsize_t[rank];
    ndims             = H5Sget_simple_extent_dims(file_dataspace_id, dims, NULL);
    num_elem          = H5Sget_simple_extent_npoints(file_dataspace_id);
    data              = new Real[num_elem];
    maxDim[1]         = dims[0];
    maxDim[0]         = dims[1];
    NCH               = dims[2];
    dataspace_id      = H5Screate_simple(rank, dims, NULL);
    int status        = H5Dread(dataset_id, HDF_PRECISION, dataspace_id, file_dataspace_id, H5P_DEFAULT, data);

    const int Nmax = std::max(maxDim[0], maxDim[1]);
    m_data.set_max3Ddim(Nmax);

    /* release stuff */
    delete [] dims;
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    status = H5Sclose(file_dataspace_id);
    status = H5Fclose(file_id);

    assert(channel < NCH);

    /* extract plane */
    m_data.set_sliceDir('s'); // from 2D slice
    m_data.set_sliceID(-1);
    m_data.resize(maxDim[0], maxDim[1]);
    if (magnitude && NCH == 3)
    {
        for (int h=0; h < m_data.height(); ++h)
            for (int w=0; w < m_data.width(); ++w)
            {
                const Real a = data[0 + NCH*(w + maxDim[0]*h)];
                const Real b = data[1 + NCH*(w + maxDim[0]*h)];
                const Real c = data[2 + NCH*(w + maxDim[0]*h)];
                m_data(w,h) = std::sqrt(a*a + b*b + c*c);
            }
    }
    else
    {
        for (int h=0; h < m_data.height(); ++h)
            for (int w=0; w < m_data.width(); ++w)
                m_data(w,h) = data[channel + NCH*(w + maxDim[0]*h)];
    }
    delete [] data;
    m_dataLoaded = true;

#else
    fprintf(stderr, "WARNING: Executable was compiled without HDF support...\n");
#endif /* _USE_HDF_ */
}

void Polaroid::load_wavelet(const char* filename, ArgumentParser& parser)
{
#ifdef _USE_CUBISMZ_
    const double sf        = parser("-sf").asDouble(0.5); // slice fraction (default)
    const int si           = parser("-si").asInt(-1); // slice index (need to specify)
    const char direction   = parser("-sd").asString("x")[0]; // slice normal direction
    const bool byte_swap   = parser("-swap").asBool(false);
    const int wavelet_type = parser("-wtype").asInt(1);

    const string fname(filename);
    Reader_WaveletCompression myreader(fname, byte_swap, wavelet_type);
    myreader.load_file();
    const int NBX = myreader.xblocks();
    const int NBY = myreader.yblocks();
    const int NBZ = myreader.zblocks();
    const int maxDim[3] = {NBX*_BLOCKSIZE_, NBY*_BLOCKSIZE_, NBZ*_BLOCKSIZE_};
    const int Nmax = std::max(maxDim[0], std::max(maxDim[1], maxDim[2]));
    m_data.set_max3Ddim(Nmax);

    Real blockdata[_BLOCKSIZE_][_BLOCKSIZE_][_BLOCKSIZE_];

    /* extract plane */
    m_data.set_sliceDir(direction);
    if ('x' == direction) // y-z plane
    {
        const int fixed = (si >= 0) ? si : static_cast<int>(maxDim[0]*sf);
        m_data.set_sliceID(fixed);
        m_data.resize(maxDim[1], maxDim[2]);

        const int fixedBID = fixed/_BLOCKSIZE_;
        const int BlocalID = fixed % _BLOCKSIZE_;
        for (int iz=0; iz < NBZ; ++iz)
            for (int iy=0; iy < NBY; ++iy)
            {
                const double zratio = myreader.load_block2(fixedBID, iy, iz, blockdata);
                for (int z=0; z < _BLOCKSIZE_; ++z)
                    for (int y=0; y < _BLOCKSIZE_; ++y)
                    {
                        assert(iy*_BLOCKSIZE_+y < m_data.width());
                        assert(iz*_BLOCKSIZE_+z < m_data.height());
                        m_data(iy*_BLOCKSIZE_+y, iz*_BLOCKSIZE_+z) = blockdata[z][y][BlocalID];
                    }
            }
    }
    else if ('y' == direction) // x-z plane
    {
        const int fixed = (si >= 0) ? si : static_cast<int>(maxDim[1]*sf);
        m_data.set_sliceID(fixed);
        m_data.resize(maxDim[0], maxDim[2]);

        const int fixedBID = fixed/_BLOCKSIZE_;
        const int BlocalID = fixed % _BLOCKSIZE_;
        for (int iz=0; iz < NBZ; ++iz)
            for (int ix=0; ix < NBX; ++ix)
            {
                const double zratio = myreader.load_block2(ix, fixedBID, iz, blockdata);
                for (int z=0; z < _BLOCKSIZE_; ++z)
                    for (int x=0; x < _BLOCKSIZE_; ++x)
                    {
                        assert(ix*_BLOCKSIZE_+x < m_data.width());
                        assert(iz*_BLOCKSIZE_+z < m_data.height());
                        m_data(ix*_BLOCKSIZE_+x, iz*_BLOCKSIZE_+z) = blockdata[z][BlocalID][x];
                    }
            }
    }
    else if ('z' == direction) // x-y plane
    {
        const int fixed = (si >= 0) ? si : static_cast<int>(maxDim[2]*sf);
        m_data.set_sliceID(fixed);
        m_data.resize(maxDim[0], maxDim[1]);

        const int fixedBID = fixed/_BLOCKSIZE_;
        const int BlocalID = fixed % _BLOCKSIZE_;
        for (int iy=0; iy < NBY; ++iy)
            for (int ix=0; ix < NBX; ++ix)
            {
                const double zratio = myreader.load_block2(ix, iy, fixedBID, blockdata);
                for (int y=0; y < _BLOCKSIZE_; ++y)
                    for (int x=0; x < _BLOCKSIZE_; ++x)
                    {
                        assert(ix*_BLOCKSIZE_+x < m_data.width());
                        assert(iy*_BLOCKSIZE_+y < m_data.height());
                        m_data(ix*_BLOCKSIZE_+x, iy*_BLOCKSIZE_+y) = blockdata[BlocalID][y][x];
                    }
            }
    }
    else
    {
        fprintf(stderr, "ERROR: Direction '%c' unsupported.", direction);
        abort();
    }

    m_dataLoaded = true;
#else
    fprintf(stderr, "WARNING: Executable was compiled without wavelet compressor support...\n");
#endif /* _USE_CUBISMZ_ */
}
