// File       : SchlierenCartridge.cpp
// Date       : Fri 29 Apr 2016 11:30:07 AM CEST
// Author     : Fabian Wermelinger
// Description: Schlieren shader implementation
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include "SchlierenCartridge.h"

void SchlierenCartridge::_compute(Slice& data)
{
    m_gradX.resize(data.width(), data.height());
    m_gradY.resize(data.width(), data.height());
    _gradX(data);
    _gradY(data);
    for (int h=0; h < data.height(); ++h)
        for (int w=0; w < data.width(); ++w)
        {
            const Real IphiI2 = m_gradX(w,h)*m_gradX(w,h) + m_gradY(w,h)*m_gradY(w,h);
            data(w,h) = sqrt(IphiI2);
        }
}

// 4th-order Finite Differences
void SchlierenCartridge::_gradX(const Slice& data)
{
    // this assumes
    // 1.) uniform grid-spacing
    // 2.) maximum extend = 1
    const Real fac = data.get_max3Ddim()/12.0;

    // left
    for (int h=0; h < data.height(); ++h)
        for (int w=0; w < 2; ++w)
            m_gradX(w,h) = fac*(-25.0*data(w,h) + 48.0*data(w+1,h) - 36.0*data(w+2,h) + 16.0*data(w+3,h) - 3.0*data(w+4,h));

    // right
    for (int h=0; h < data.height(); ++h)
        for (int w=data.width()-2; w < data.width(); ++w)
            m_gradX(w,h) = -fac*(-25.0*data(w,h) + 48.0*data(w-1,h) - 36.0*data(w-2,h) + 16.0*data(w-3,h) - 3.0*data(w-4,h));

    // interior
    for (int h=0; h < data.height(); ++h)
        for (int w=2; w < data.width()-2; ++w)
            m_gradX(w,h) = fac*(-data(w+2,h) + 8.0*data(w+1,h) - 8.0*data(w-1,h) + data(w-2,h));
}

void SchlierenCartridge::_gradY(const Slice& data)
{
    // this assumes
    // 1.) uniform grid-spacing
    // 2.) maximum extend = 1
    const Real fac = data.get_max3Ddim()/12.0;

    // left
    for (int h=0; h < 2; ++h)
        for (int w=0; w < data.width(); ++w)
            m_gradY(w,h) = fac*(-25.0*data(w,h) + 48.0*data(w,h+1) - 36.0*data(w,h+2) + 16.0*data(w,h+3) - 3.0*data(w,h+4));

    // right
    for (int h=data.height()-2; h < data.height(); ++h)
        for (int w=0; w < data.width(); ++w)
            m_gradY(w,h) = -fac*(-25.0*data(w,h) + 48.0*data(w,h-1) - 36.0*data(w,h-2) + 16.0*data(w,h-3) - 3.0*data(w,h-4));

    // interior
    for (int h=2; h < data.height()-2; ++h)
        for (int w=0; w < data.width(); ++w)
            m_gradY(w,h) = fac*(-data(w,h+2) + 8.0*data(w,h+1) - 8.0*data(w,h-1) + data(w,h-2));
}
