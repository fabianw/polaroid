// File       : PhotoINFO.h
// Date       : Tue 12 Jul 2016 04:17:47 PM CEST
// Author     : Fabian Wermelinger
// Description: Statistics photo
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef PHOTOINFO_H_SVPPOILX
#define PHOTOINFO_H_SVPPOILX

#include <string>
#include <sstream>
#include "common.h"
#include "PhotoPaper.h"
#include "Types.h"

class PhotoINFO: public PhotoPaper
{
    std::ostringstream m_buf;

public:
    PhotoINFO(const std::string filename="ascii") : PhotoPaper(0,0,filename)
    {
        m_description = "ASCII information";
    }
    virtual ~PhotoINFO() { }

    virtual void make_new(const std::string name, const int dummy1=0, const int dummy2=0);
    virtual void resize(const int dummy1, const int dummy2) { }
    virtual void write() { }
    virtual void set_pixel(const double dummy1, const int dummy2, const int dummy3) { }
    virtual std::string suffix() const { return std::string(".info"); }
    void write(const Slice2D_Statistics& stat);
};

#endif /* PHOTOINFO_H_SVPPOILX */
