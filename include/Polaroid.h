// File       : Polaroid.h
// Date       : Tue Apr 26 14:41:37 2016
// Author     : Fabian Wermelinger
// Description: Polaroid Camera base
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef POLAROID_H_HXFL9YPG
#define POLAROID_H_HXFL9YPG

#include "Types.h"
#include "Cartridge.h"
#include "ArgumentParser.h"

class PhotoPaper;

class Polaroid
{
protected:
    Slice m_data;
    bool m_dataLoaded;

    // Cartridge
    Cartridge* m_cartridge;
    bool m_cartridgeInserted;

public:
    Polaroid() : m_dataLoaded(false), m_cartridge(nullptr), m_cartridgeInserted(false) { }
    Polaroid(Cartridge* cart) : m_dataLoaded(false), m_cartridge(cart), m_cartridgeInserted(true) { }
    ~Polaroid() {}

    // cartridge loader
    inline void insertCartridge(Cartridge* cart)
    {
        // memory management of cartridges is not handled by the Polaroid cam
        m_cartridge = cart;
        m_cartridgeInserted = true;
    }

    // scene loader
    void load_hdf5(const char* filename, ArgumentParser& parser);
    void load_hdf5_slice(const char* filename, ArgumentParser& parser);
    void load_wavelet(const char* filename, ArgumentParser& parser);

    // capture scene
    inline void capture(PhotoPaper& photo)
    {
        if (m_dataLoaded && m_cartridgeInserted)
            m_cartridge->capture(photo, m_data);
    }
    inline void computeScene()
    {
        if (m_dataLoaded && m_cartridgeInserted)
            m_cartridge->compute(m_data);
    }

    // data accessors
    inline int width() const { return m_data.width(); }
    inline int height() const { return m_data.height(); }
    inline Real min() const { return m_data.min(); }
    inline Real max() const { return m_data.max(); }
};

#endif /* POLAROID_H_HXFL9YPG */
